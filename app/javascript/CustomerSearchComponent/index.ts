import { Component } from "@angular/core";
import { Http } from "@angular/http";
import template from './template.html';

const CustomerSearchComponent = Component({
 selector: 'shine-customer-search',
 template:  template
}).Class({
 constructor: [
  Http,
  function(http) {
   this.customers = null;
   this.http      = http;
   this.keywords  = "";
  }
 ],
 search($event) {
  this.keywords = $event;
  if (this.keywords.length < 3) {
   return;
  }
  this.http.get(
      "/customers.json?keywords=" + this.keywords
  ).subscribe(
      response =>  this.customers = response.json().customers,
      response =>  alert(response)
  );
 }
});




 export { CustomerSearchComponent };