module.exports = {
    test: /\.slim$/,
    enforce: 'pre',
    exclude: /node_modules/,
    loader: 'slim-lang-loader',
    options: {
        runner: 'bin/rails runner'
    }
}
